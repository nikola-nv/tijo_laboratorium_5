package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.converter.Converter;
import pl.edu.pwsztar.domain.dto.CreateMovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

@Component
public class MovieMapper implements Converter<CreateMovieDto,Movie> {

    @Override
    public Movie convert (CreateMovieDto movieDto){
        return new Movie(movieDto.getTitle(),movieDto.getImage(),movieDto.getYear());
    }

}

