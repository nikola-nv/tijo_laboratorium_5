package pl.edu.pwsztar.domain.mapper;

import org.springframework.stereotype.Component;
import pl.edu.pwsztar.converter.Converter;
import pl.edu.pwsztar.domain.dto.MovieDto;
import pl.edu.pwsztar.domain.entity.Movie;

import java.util.List;
import java.util.stream.Collectors;


@Component
public class MovieListMapper implements Converter<List<Movie>,List<MovieDto>> {
    @Override
    public List<MovieDto> convert(List<Movie> movies){
        List<MovieDto> moviesDto = movies
                .stream()
                .map(movie -> {
                    MovieDto movieDto = new MovieDto();

                    movieDto.setImage(movie.getImage());
                    movieDto.setTitle(movie.getTitle());
                    movieDto.setYear(movie.getYear());
                    movieDto.setMovieId(movie.getMovieId());

                    return movieDto;
                })
                .collect(Collectors.toList());

        return moviesDto;
    }
}
